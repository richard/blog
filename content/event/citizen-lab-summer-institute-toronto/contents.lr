title: Citizen Lab Summer Institute (Toronto)
---
author: steph
---
start_date: 2018-06-13
---
end_date: 2018-06-15
---
body:

he Citizen Lab Summer Institute on Monitoring Internet Openness and Rights is an annual research workshop hosted at the Munk School of Global Affairs, University of Toronto.

CLSI is a workshop for researchers and practitioners from academia, civil society, and the private sector who are working on Internet openness, security, and rights. The event brings together people with different perspectives from a wide range of backgrounds across technical and social science disciplines. Participants range from established experts to those just entering the area. We started CLSI to demonstrate that a greater understanding of technology and policy can only be achieved through interdisciplinary collaboration. Our aim is to help build and support a community that shares this belief.

CLSI is not your average academic workshop. The goal is to form collaborations and work together on projects through intensive participant-led sessions. CLSI provides a unique opportunity to meet a diverse group of researchers, practitioners, and advocates and develop new and exciting research that addresses pressing questions for the Internet.

Collaborations formed at prior CLSI workshops have led to high impact projects including: [analyzing national security and signals intelligence policy in Canada](https://citizenlab.ca/2017/12/citizen-lab-and-cippic-release-analysis-of-the-communications-security-establishment-act/) (2017), [investigating censorship of the death of Liu Xiaobo on WeChat and Weibo](https://citizenlab.ca/2017/07/analyzing-censorship-of-the-death-of-liu-xiaobo-on-wechat-and-weibo/) (2017), [conducting security audits of child monitoring apps in South Korea](https://netalert.me/safer-without.html) (2017, 2016, 2015), [documenting Internet filtering in Zambia](https://ooni.torproject.org/post/zambia-election-monitoring/) (2016), and [exposing the “Great Cannon”](https://citizenlab.org/2015/04/chinas-great-cannon/) (2014), an attack tool in China used for large scale distributed-denial of service attacks against Github and GreatFire.org.

**The 2018 Citizen Lab Summer Institute will be held at the University of Toronto on June 13-15, 2018**

Website:
https://citizenlab.ca/summerinstitute/2018.html
