title: PrivChat #4 - 25th Anniversary of Onion Routing
---
author: alsmith
---
start_date: 2021-05-26
---
summary:

To celebrate the 25th anniversary of the academic publication of onion routing, we're holding PrivChat #4 on May 26 at 18:00 UTC with host Gabriella Coleman and panelists Paul Syverson, Roger Dingledine and Nick Mathewson.



---
body:


**Wednesday, May 26 ∙ 18:00 UTC ∙ 14:00 Eastern ∙ 11:00 Pacific** 

Celebrate 25 years of onion routing with Tor! 

May 31, 2021 marks the 25th anniversary of the first public presentation of onion routing in Cambridge, UK at Isaac Newton Institute's first Information Hiding Workshop. 

You’re invited to celebrate this special moment with us to talk about the beginnings of onion routing, and how this idea became Tor, and how the Tor Project eventually came to be. We'll be joined by **Paul Syverson**, one of the authors of the first onion routing paper, together with the Tor Project co-founders **Roger Dingledine** and **Nick Mathewson**. 

We’ll reflect on the first days of the onion routing network at the U.S. Naval Research Lab (NRL) -- where Paul, Roger, and Nick worked together. (Back then, onion router connections went through five nodes instead of Tor’s current three-nodes design!) It's no secret that the concept of onion routing originated at NRL (it’s on our history page), but there is so much more we want to share about how Tor started and where we’ve come in the last 25 years. 

**Gabriella Coleman** -- anthropologist, author, and Tor board member -- will join us as our host and moderator. 

Join us for a celebratory edition of PrivChat to commemorate the 25th anniversary of onion routing.

Website:
https://www.youtube.com/watch?v=-wbivkG8TcU
