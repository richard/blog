title: Run and use Tor Bridges to bypass censorship and surveillance! (Online)
---
author: ggus
---
start_date: 2020-05-29
---
body:

On **May 29th 2020**, members of The Tor Project will be online to support and explain how to help users in countries that censor the Internet. From 1200 UTC to 2000 UTC, **we will teach you how to run a Tor bridge** and answer questions about our anti-censorship technology. **This event is public and open for everyone!**

The event will happen via text (not video or audio) on IRC, in the channel #tor-relays on irc.oftc.net. Learn here [how to connect to our IRC](https://support.torproject.org/get-in-touch/).

**About bridges**

Bridges are useful for Tor users living in oppressive regimes, and for people who want an extra layer of security because they're worried somebody will recognize that they are contacting a public Tor relay IP address. A bridge is basically a private Tor relay that helps you hide the fact that you are using the Tor network. To set up a bridge, take a look at the instructions in our [Community portal](https://community.torproject.org/relay/setup/bridge/).

**About this event**

This is our first online event promoting bridges and anti-censorship technology and  you can join at any time between 1200 UTC and 2000 UTC. Several members of The Tor Project will be around to answer questions and provide technical support related to bridges and anti-censorship technology.

We expect to answer your questions and teach attendees how they can help friends in censored countries, how they can set up Tor bridges, and what else they can do to fight online censorship.

**Learn more**

- Tor Browser User Manual: [How to use bridges](https://tb-manual.torproject.org/bridges/) and [Censorship circumvention.](https://tb-manual.torproject.org/circumvention/)

- [Censorship section](https://support.torproject.org/censorship/) on Support portal.

- [Relay Operators section](https://community.torproject.org/relay/) on Community portal.

- [Snowflake](https://snowflake.torproject.org/)  



---
tags:

meetup
censorship circumvention
