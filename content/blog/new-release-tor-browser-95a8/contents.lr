title: New Release: Tor Browser 9.5a8
---
pub_date: 2020-03-12
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-9.5
---
categories: applications
---
_html_body:

<p>Tor Browser 9.5a8 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/9.5a8/">distribution directory</a>.</p>
<p>This release features important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2020-09/">security updates</a> to Firefox.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-906">latest stable release</a> instead.</p>
<p>This release updates Firefox to 68.6.0esr and NoScript to 11.0.15.</p>
<p><strong>Note</strong>: We were made aware of a bug that allows javascript execution on the Safest security level (in some situations). We are working on a fix for this. If you require that javascript is blocked, then you may completely disable it by:</p>
<ul>
<li>Open about:config</li>
<li>Search for: javascript.enabled</li>
<li>If the "Value" column says "false", then javascript is already disabled.</li>
<li>If the "Value" column says "true", then either right-click and select "Toggle" such that it is now disabled or double-click on the row and it will be disabled.</li>
</ul>
<p><b>Update</b>: Noscript 11.0.17 should solve this issue. Automatic updates of Noscript are enabled by default, so you should get this fix automatically.</p>
<p>If your Tor Browser Alpha installation did not upgrade to version 9.5a7 and your current installation does not start successfully, then please see the <a href="https://blog.torproject.org/new-release-tor-browser-95a7">9.5a7 blog post</a> for solutions.</p>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">changelog</a> since Tor Browser 9.5a7 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 68.6.0esr</li>
<li>Bump NoScript to 11.0.15
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/33430">Bug 33430</a>: Disable downloadable fonts on Safest security level</li>
</ul>
</li>
<li>Translations update</li>
</ul>
</li>
<li>Windows + OS X + Linux
<ul>
<li>Update Tor Launcher to 0.2.21.4
<ul>
<li>Translations update</li>
</ul>
</li>
</ul>
</li>
<li>Build System
<ul>
<li>Windows
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/33535">Bug 33535</a>: Patch openssl to use SOURCE_DATE_EPOCH for copyright year</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-287037"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287037" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="distribution directory Link">distribution d… (not verified)</span> said:</p>
      <p class="date-time">March 14, 2020</p>
    </div>
    <a href="#comment-287037">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287037" class="permalink" rel="bookmark">Hi
Please correct …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi<br />
Please correct "distribution directory" Link<br />
Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287054"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287054" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">March 16, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287037" class="permalink" rel="bookmark">Hi
Please correct …</a> by <span content="distribution directory Link">distribution d… (not verified)</span></p>
    <a href="#comment-287054">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287054" class="permalink" rel="bookmark">Fixed, thanks.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Fixed, thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287041"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287041" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 14, 2020</p>
    </div>
    <a href="#comment-287041">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287041" class="permalink" rel="bookmark">distribution directory
…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>distribution directory</p></blockquote>
<p>points to <a href="https://www.torproject.org/dist/torbrowser/9.5a7/" rel="nofollow">https://www.torproject.org/dist/torbrowser/9.5a7/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287055"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287055" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">March 16, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287041" class="permalink" rel="bookmark">distribution directory
…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287055">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287055" class="permalink" rel="bookmark">Fixed, thanks.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Fixed, thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287044"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287044" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>G_man (not verified)</span> said:</p>
      <p class="date-time">March 14, 2020</p>
    </div>
    <a href="#comment-287044">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287044" class="permalink" rel="bookmark">What about fonts? I have…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about fonts? I have checked about: config downloadable fonts are still enabled?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287056"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287056" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">March 16, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287044" class="permalink" rel="bookmark">What about fonts? I have…</a> by <span>G_man (not verified)</span></p>
    <a href="#comment-287056">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287056" class="permalink" rel="bookmark">What did you check in about…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What did you check in about:config?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287045"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287045" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Drop_code (not verified)</span> said:</p>
      <p class="date-time">March 15, 2020</p>
    </div>
    <a href="#comment-287045">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287045" class="permalink" rel="bookmark">I checked the downloadable…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I checked the downloadable fonts, they are still active, the gfx and woff2.Am i looking the right option?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287051"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287051" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>none (not verified)</span> said:</p>
      <p class="date-time">March 16, 2020</p>
    </div>
    <a href="#comment-287051">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287051" class="permalink" rel="bookmark">distribution directory links…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>distribution directory links is for older version</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287057"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287057" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">March 16, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287051" class="permalink" rel="bookmark">distribution directory links…</a> by <span>none (not verified)</span></p>
    <a href="#comment-287057">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287057" class="permalink" rel="bookmark">Fixed, thanks.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Fixed, thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287066"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287066" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>GreenReaper (not verified)</span> said:</p>
      <p class="date-time">March 16, 2020</p>
    </div>
    <a href="#comment-287066">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287066" class="permalink" rel="bookmark">The &quot;about this release&quot;…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The "about this release" link on Google Play is to <a href="https://blog.torproject.org/new-release-tor-browser-95a8" rel="nofollow">https://blog.torproject.org/new-release-tor-browser-95a8</a> but this page is <a href="https://blog.torproject.org/new-release-tor-browser-958" rel="nofollow">https://blog.torproject.org/new-release-tor-browser-958</a> (no 'b' before the 8).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287076"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287076" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">March 17, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287066" class="permalink" rel="bookmark">The &quot;about this release&quot;…</a> by <span>GreenReaper (not verified)</span></p>
    <a href="#comment-287076">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287076" class="permalink" rel="bookmark">Thanks. The 95a8 link should…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks. The 95a8 link should now be working.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287093"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287093" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 18, 2020</p>
    </div>
    <a href="#comment-287093">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287093" class="permalink" rel="bookmark">Why can&#039;t i update under…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why can't i update under linux ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287105"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287105" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">March 19, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287093" class="permalink" rel="bookmark">Why can&#039;t i update under…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287105">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287105" class="permalink" rel="bookmark">What happens when you try to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What happens when you try to update?</p>
<p>If you go to hamburger menu -&gt; help -&gt; about tor browser, does it tell you that a new version is available?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287108"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287108" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Fernando  (not verified)</span> said:</p>
      <p class="date-time">March 20, 2020</p>
    </div>
    <a href="#comment-287108">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287108" class="permalink" rel="bookmark">Muito top
Essa rede é uma…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Muito top<br />
Essa rede é uma das melhores que ja vi...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287111"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287111" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>justoneinthecrowd (not verified)</span> said:</p>
      <p class="date-time">March 20, 2020</p>
    </div>
    <a href="#comment-287111">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287111" class="permalink" rel="bookmark">Got some NoScript XSS…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Got some NoScript XSS Warnings when browsing, for that of an error.</p>
<p>Any fix being offered here?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-287285"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287285" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>KPM (not verified)</span> said:</p>
      <p class="date-time">March 27, 2020</p>
    </div>
    <a href="#comment-287285">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287285" class="permalink" rel="bookmark">Does custom obfs4 bridges…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does custom obfs4 bridges work on Tor Browser 9.5a8 for Android.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287322"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287322" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">March 30, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287285" class="permalink" rel="bookmark">Does custom obfs4 bridges…</a> by <span>KPM (not verified)</span></p>
    <a href="#comment-287322">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287322" class="permalink" rel="bookmark">It should work yes. This was…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It should work yes. This was fixed with this ticket:<br />
<a href="https://trac.torproject.org/projects/tor/ticket/30767" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/30767</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287330"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287330" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>KPM (not verified)</span> said:</p>
      <p class="date-time">March 30, 2020</p>
    </div>
    <a href="#comment-287330">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287330" class="permalink" rel="bookmark">Tor Browser 9.5.a8 also…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Browser 9.5.a8 also doesn't work on a Samsung Galaxy S2.</p>
<p><a href="https://trac.torproject.org/projects/tor/ticket/32348" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/32348</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
