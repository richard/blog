title: The Wilmington Watch: A Tor Network Team Hackfest
---
pub_date: 2017-07-05
---
author: asn
---
tags:

hackfest
development
---
categories:

community
devops
---
_html_body:

<hr />
<p>The <a href="https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam">Tor network team</a> is a small team responsible for developing the core Tor daemon. We're located around the globe, so we periodically meet in person for team hackfests to keep our team fresh and up to date with all things Tor, and to fast-track features and improvements. Previously, we've met for hackfests in <a href="https://blog.torproject.org/blog/hidden-service-hackfest-arlington-accords">Arlington</a> and <a href="https://blog.torproject.org/blog/mission-montreal-building-next-generation-onion-services">Montreal</a>, and for our latest meeting, we met in Wilmington, Delaware, a town revered for its yearly Italian Festival.</p>
<p class="text-align-center"><a href="https://extra.torproject.org/blog/2017-07-02-wilmington-hackfest/delaware_river.png"><img alt-text="Wilmington, Delaware" height="450" src="https://extra.torproject.org/blog/2017-07-02-wilmington-hackfest/delaware_river.png" /></a></p>
<p>To better understand the geographical setting of this meeting, go to Trenton, New Jersey, USA, hop into a Delaware river riverboat, then follow the flow south; after about 60 miles you will eventually see a town named Wilmington on your right. This small town hosted us for a few days while we worked on making Tor stronger and safer.</p>
<h3>What went down </h3>
<p>We worked intensely for several days and nights, researching, planning, and cooking meals for each other. Here is a small fraction of the topics we worked on:</p>
<ul>
<li>
<p>We schemed on about the <a href="https://trac.torproject.org/projects/tor/query?status=!closed&amp;keywords=~tor-modularity">Tor modularization project</a> which aims to clean up and organize <a href="https://gitweb.torproject.org/tor.git">our codebase</a> into nice and tidy abstract modules. Cleaning and modularizing our code not only reduces technical debt, but also allows us to eventually rewrite those submodules into higher-level languages such as <a href="https://en.wikipedia.org/wiki/Rust_(programming_language)">Rust</a>, <a href="https://en.wikipedia.org/wiki/D_(programming_language)">D</a> or <a href="https://en.wikipedia.org/wiki/APL_(programming_language)">APL</a>.</p>
</li>
</ul>
<p><img alt="Tor network team hackfest stickies" src="/static/images/blog/inline-images/tor-hackfest-wilmington-stickies.jpg" class="align-center" /></p>
<ul>
<li>That last part is not science-fiction; as of a month ago, Tor's build system <a href="https://blog.torproject.org/blog/tor-0312-alpha-out-notes-about-0311-alpha">is capable</a> of building and linking code modules written in Rust. We <a href="https://trac.torproject.org/projects/tor/ticket/22106">are now actively working</a> on implementing the <a href="https://gitweb.torproject.org/torspec.git/tree/proposals/264-subprotocol-versions.txt">protover</a> <a href="https://gitweb.torproject.org/tor.git/tree/src/or/protover.c">subsystem</a> into Rust as our first prototype module.</li>
</ul>
<ul>
<li>As part of the security discussion, we talked about the <a href="https://gitweb.torproject.org/torspec.git/tree/proposals/251-netflow-padding.txt">new padding defenses</a> that were recently <a href="https://trac.torproject.org/projects/tor/ticket/16861">added to Tor</a> and provide cover to Tor circuits against traffic analysis. We made plans for future padding techniques and defenses.</li>
<li>
<p>We also briefed up the whole team on how our <a href="https://gitweb.torproject.org/torspec.git/tree/guard-spec.txt">new entry guard picking algorithm</a> works to enhance the security of Tor clients and protects them against local network attacks. We planned various defenses against <a href="https://gitweb.torproject.org/torspec.git/tree/proposals/247-hs-guard-discovery.txt">hidden service guard discovery attacks</a>, as well as <a href="https://www.freehaven.net/anonbib/papers/pets2013/paper_65.pdf">alternative onion routing path algorithms</a>. Our next step for improving guard security is to simulate alternative path construction algorithms and evaluate their performance and security guarantees.</p>
</li>
<li>We discussed KIST, an alternative network scheduler and congestion management logic for Tor which offers improved circuit performance and cleaner network tubes. KIST is currently <a href="https://trac.torproject.org/projects/tor/ticket/12541">under active development</a>, so we roadmapped and planned for how to get it included upstream.</li>
</ul>
<ul>
<li>We talked about our code testing techniques and how to improve them. We discussed the necessity of <a href="https://trac.torproject.org/projects/tor/ticket/22745">regression tests</a>, the need to improve our integration tests, and also the future of our <a href="https://gitweb.torproject.org/tor.git/tree/doc/HACKING/Fuzzing.md">fuzzing framework</a>. (Feel free to <a href="https://www.torproject.org/about/contact.html.en#irc">get in touch</a> if you want to help improve our tests!)</li>
</ul>
<ul>
<li>
<p>We all shared our experiences and thoughts on our beloved tool for ticket tracking and project management: <a href="https://trac.torproject.org/">Trac</a>. We discussed ways we could improve our Trac workflows and also alternative tools we could potentially try (e.g. Gitlab). Transitioning to another tool is not so easy, though; since our Trac instance contains 10+ years of Tor history, we need to make sure we don't lose any information.</p>
</li>
</ul>
<h3 class="text-align-center"><a href="https://extra.torproject.org/blog/2017-07-02-wilmington-hackfest/alienabduction.jpg"><img alt-text:="" height="400" src="https://extra.torproject.org/blog/2017-07-02-wilmington-hackfest/alienabduction.jpg" /></a></h3>
<p>We stayed for about 5 days in town doing all these things and more. Then on a Friday afternoon as the Wilmington Italian Festival was setting up for yet another day, we jumped on trains, planes, and buses and moved on to other places and stories. Life goes on, and the same goes for Tor development.</p>
<p>We're committed to being open and transparent about our work, and we hope you enjoyed this post. Keep on hacking.</p>
<h3>Sponsor your own hackfest</h3>
<p>If you find these sort of hackfests exciting, and you would like to host or sponsor one, don't hesitate to get in touch us at <a href="mailto:press@torproject.org">press@torproject.org</a> with "Hackfest" in the subject line.</p>

---
_comments:

<a id="comment-269682"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269682" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="True Badass Banana (confirmed) -- despite of --">True Badass Ba… (not verified)</span> said:</p>
      <p class="date-time">July 05, 2017</p>
    </div>
    <a href="#comment-269682">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269682" class="permalink" rel="bookmark">Uhhh... I couldn&#039;t figure…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Uhhh... I couldn't figure out how Nelson Mandela and Alien Abduction pictures fit into the theme of the post. xD</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269690"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269690" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 05, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269682" class="permalink" rel="bookmark">Uhhh... I couldn&#039;t figure…</a> by <span content="True Badass Banana (confirmed) -- despite of --">True Badass Ba… (not verified)</span></p>
    <a href="#comment-269690">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269690" class="permalink" rel="bookmark">The Nelson Mandela paintings…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The Nelson Mandela paintings were on the wall in the room where we did the brainstorming and writing-on-pieces-of-paper. So when we moved to a new piece of paper, we put the old one up on the wall, and of course you need to put them up *around* the paintings, not over them, to make sure not to damage them.</p>
<p>Alien Abduction, on the other hand, was one of the rides at the Italian festival that people went to one of the evenings.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269698"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269698" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Badass Banana (not verified)</span> said:</p>
      <p class="date-time">July 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-269698">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269698" class="permalink" rel="bookmark">Ahhhh... I hope many Chinese…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ahhhh... I hope many Chinese cyber-strategists will be reading this post, so they may be aware of which kind of person they are facing in "the Sino-Tor war over the Great Firewall". xD</p>
<p>Don't forget a white-plastic board + marker pens of different colors. I find that extremely useful in group meetings of discussion/study. xD</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-269693"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269693" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>WikiMedia Executive (not verified)</span> said:</p>
      <p class="date-time">July 05, 2017</p>
    </div>
    <a href="#comment-269693">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269693" class="permalink" rel="bookmark">We discussed KIST, an…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>We discussed KIST, an alternative network scheduler and congestion management logic for Tor which offers improved circuit performance and cleaner network tubes.</p></blockquote>
<p>Interesting... in practice will it have a significant impact on Tor's speed?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269699"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269699" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  asn
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">George Kadianakis</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/33">George Kadianakis</a> said:</p>
      <p class="date-time">July 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269693" class="permalink" rel="bookmark">We discussed KIST, an…</a> by <span>WikiMedia Executive (not verified)</span></p>
    <a href="#comment-269699">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269699" class="permalink" rel="bookmark">Hey, I don&#039;t know the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hey, I don't know the precise values here but KIST indeed helps with congestion management on relays, that is relays that are overloaded will schedule their circuit handling better.</p>
<p>Also, KIST is a simplified version of our currently circuit scheduler, which will make our codebase cleaner and more readable. Which is another plus.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-269706"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269706" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  pastly
  </article>
    <div class="comment-header">
      <p class="comment__submitted">pastly said:</p>
      <p class="date-time">July 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269693" class="permalink" rel="bookmark">We discussed KIST, an…</a> by <span>WikiMedia Executive (not verified)</span></p>
    <a href="#comment-269706">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269706" class="permalink" rel="bookmark">KIST prioritizes &quot;web&quot;…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>KIST prioritizes "web" circuits (low bandwidth usage, traffic only periodically in small bursts) over "bulk" circuits (high bandwidth usage, constant traffic). When a relay is choosing which circuit to pull cells from next to send out into the network, with KIST it will pick "web" circuits before "bulk" ones. For more information on Tor's shortcomings (as of a few years ago) and how KIST works, see <a href="http://www.robgjansen.com/publications/kist-sec2014.pdf" rel="nofollow">the KIST paper</a>.</p>
<p>I am cautiously optimistic based on our large scale simulation results. Long live network tests are wrapping up. I expect to be able to update the KIST trac tickets soon.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269707"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269707" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to pastly</p>
    <a href="#comment-269707">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269707" class="permalink" rel="bookmark">Thanks a lot pastly for the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks a lot pastly for the non-technical breakdown!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-269721"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269721" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to pastly</p>
    <a href="#comment-269721">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269721" class="permalink" rel="bookmark">The safest way to obtain Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The safest way to obtain Tor Browser still appears to be to download and verify the tarballs from <a href="http://www.torproject.org" rel="nofollow">www.torproject.org</a>, using a previous TB (or Tails).  Would KIST discriminate against downloading the latest TB tarball--- or even worse, the latest Tails iso image?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269745"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269745" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  pastly
  </article>
    <div class="comment-header">
      <p class="comment__submitted">pastly said:</p>
      <p class="date-time">July 07, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269721" class="permalink" rel="bookmark">The safest way to obtain Tor…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-269745">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269745" class="permalink" rel="bookmark">In the experiments conducted…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In the experiments conducted so far (and there have been a ton...) I have not seen "bulk" downloads getting slowed down.</p>
<p>While it is true on the micro-scale of individual scheduling decisions (of which there are 100 per second) that "web" circuits will be written to the kernel first, it appears that on the macro scale of time-to-last-byte that "bulk" circuits are not any slower. Part of the reason why could be that "bulk" circuits want to be scheduled every single time but "web" circuits only want scheduling every now and then. There may be plenty of times when there are only "bulk" circuits that need scheduling. KIST also seems to be able to better utilize the available network capacity, meaning KIST <strong>might</strong> make the Tor network a little faster (this is not a claim I'm comfortable making strongly at this time). That could also explain why "bulk" circuits don't seem to be slowed: they <em>are</em> being adversely affected by the prioritization but a faster Tor balances that out. </p>
<p>Just two theories.</p>
<p>In short, I don't predict large downloads like a Tor tarball or a Tails iso taking much longer, if longer at all.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-269694"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269694" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Harm (not verified)</span> said:</p>
      <p class="date-time">July 05, 2017</p>
    </div>
    <a href="#comment-269694">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269694" class="permalink" rel="bookmark">How will the transition to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How will the transition to Rust affect the ability to compile from source?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269700"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269700" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  asn
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">George Kadianakis</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/33">George Kadianakis</a> said:</p>
      <p class="date-time">July 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269694" class="permalink" rel="bookmark">How will the transition to…</a> by <span>Harm (not verified)</span></p>
    <a href="#comment-269700">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269700" class="permalink" rel="bookmark">Initially, Rust support will…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Initially, Rust support will be optional (like now) so not much will change.</p>
<p>In the future, if Rust becomes more widespread and Tor ends up using it more, we might really bake it into Tor, and at that point you will need the Rust toolkit to compile Tor. I think we are pretty far from this goal right now tho.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-269696"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269696" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Han Cuperus (not verified)</span> said:</p>
      <p class="date-time">July 06, 2017</p>
    </div>
    <a href="#comment-269696">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269696" class="permalink" rel="bookmark">Can you tell me if a TVFrog…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you tell me if a TVFrog would do his work through a TOR-circuit in China and can so receive ALL the TV's-programs in the world for FREE, Netflix, etc already don't like the small apparate from $25-$70.<br />
Thanks for answering, ps they do it by "streaming" the internet, so no more cable-tv.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269702"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269702" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Badass Banana (not verified)</span> said:</p>
      <p class="date-time">July 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269696" class="permalink" rel="bookmark">Can you tell me if a TVFrog…</a> by <span>Han Cuperus (not verified)</span></p>
    <a href="#comment-269702">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269702" class="permalink" rel="bookmark">Side note. For the sake of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Side note. For the sake of being "FREE": if you take it from "FREEDOM", that would always be great; if you take it to be "priceless", then you will probably crash into many problems.</p>
<p><em><strong>Nothing "free", freedom is not "free"!</strong></em></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-269705"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269705" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 06, 2017</p>
    </div>
    <a href="#comment-269705">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269705" class="permalink" rel="bookmark">Hello asn, great write up as…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello asn, great write up as always! One question concerning Rust: Since AFAIU it provides parallelism, would one easy shortcut to have Tor be fully multi-threaded is to, as an example, make a relay crypto module in Rust?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269723"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269723" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  isis
  </article>
    <div class="comment-header">
      <p class="comment__submitted">isis said:</p>
      <p class="date-time">July 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269705" class="permalink" rel="bookmark">Hello asn, great write up as…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-269723">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269723" class="permalink" rel="bookmark">Hello! While Rust does allow…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello! While Rust does allow one to write memory-safe code in a concurrent manner, it doesn't automatically provide it. There's <a href="https://blog.rust-lang.org/2015/04/10/Fearless-Concurrency.html" rel="nofollow">a really good blog post</a> on rust-lang.org that explains how Rust's concept of <em>ownership</em> and memory-safety ties into paradigms for writing concurrent programs. For Tor's case, however, there's some (likely large) software architectural changes needed in Tor's codebase to be ready for being more highly-parallelisable. If anything, ownership and borrowing in Rust will make it <em>easier</em> moving forward to expand upon, and make more safe, <a href="https://gitweb.torproject.org/tor.git/tree/src/common/workqueue.c?id=86eb63deb45f2d21375bee74ef4758c24974a97e" rel="nofollow">Tor's message-passing/worker-pool multi-threading design</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-269739"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269739" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>wikileaker (not verified)</span> said:</p>
      <p class="date-time">July 07, 2017</p>
    </div>
    <a href="#comment-269739">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269739" class="permalink" rel="bookmark">go read wikileaks so many…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>go read wikileaks so many exploits</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-269791"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269791" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="A Random Troublemaker">A Random Troub… (not verified)</span> said:</p>
      <p class="date-time">July 09, 2017</p>
    </div>
    <a href="#comment-269791">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269791" class="permalink" rel="bookmark">Why Nelson Mandela? He was…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why Nelson Mandela? He was not peaceful, nor was he a statesman. He was a tyrant to turned to violence, including against non-Communist black citizens of South Africa. There are better historical figures to consider than a man who embraced violence and used it to kill children and other black Africans.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269801"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269801" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  pastly
  </article>
    <div class="comment-header">
      <p class="comment__submitted">pastly said:</p>
      <p class="date-time">July 10, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269791" class="permalink" rel="bookmark">Why Nelson Mandela? He was…</a> by <span content="A Random Troublemaker">A Random Troub… (not verified)</span></p>
    <a href="#comment-269801">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269801" class="permalink" rel="bookmark">Those pictures were already…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Those pictures were already hanging on the wall in the meeting place.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-269842"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269842" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>josh (not verified)</span> said:</p>
      <p class="date-time">July 15, 2017</p>
    </div>
    <a href="#comment-269842">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269842" class="permalink" rel="bookmark">padding defenses? According…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>padding defenses? According to the FAQ I thought you guys were against padding don't get me wrong though i'm excited that your putting some padding.</p>
<p><a href="https://www.torproject.org/docs/faq.html.en" rel="nofollow">https://www.torproject.org/docs/faq.html.en</a> </p>
<p> According to your FAQ</p>
<p>Cover traffic is really expensive. And *every* user needs to be doing it. This adds up to a lot of extra bandwidth cost for our volunteer operators, and they're already pushed to the limit.</p>
<p>You'd need to always be sending traffic, meaning you'd need to always be online. Otherwise, you'd need to be sending end-to-end cover traffic -- not just to the first hop, but all the way to your final destination -- to prevent the adversary from correlating presence of traffic at the destination to times when you're online. What does it mean to send cover traffic to -- and from -- a web server? That is not supported in most protocols.</p>
<p>Even if you *could* send full end-to-end padding between all users and all destinations all the time, you're *still* vulnerable to active attacks that block the padding for a short time at one end and look for patterns later in the path.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269870"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269870" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  asn
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">George Kadianakis</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/33">George Kadianakis</a> said:</p>
      <p class="date-time">July 17, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269842" class="permalink" rel="bookmark">padding defenses? According…</a> by <span>josh (not verified)</span></p>
    <a href="#comment-269870">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269870" class="permalink" rel="bookmark">Thanks for pointing this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for pointing this inconsistency out. I opened a trac ticket about it: <a href="https://trac.torproject.org/projects/tor/ticket/22958" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/22958</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-269871"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269871" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 17, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269842" class="permalink" rel="bookmark">padding defenses? According…</a> by <span>josh (not verified)</span></p>
    <a href="#comment-269871">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269871" class="permalink" rel="bookmark">Note that it&#039;s just netflow…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Note that it's just netflow padding to collapse netflow records, it's not the type of expensive padding that the FAQ addresses, but yes the FAQ should be tweaked a bit.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-269872"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269872" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>IPV6 Exit Nodes? (not verified)</span> said:</p>
      <p class="date-time">July 17, 2017</p>
    </div>
    <a href="#comment-269872">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269872" class="permalink" rel="bookmark">I have IPV6 disabled on my…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have IPV6 disabled on my LAN and my router.[1]</p>
<p>Why all of a sudden is Tor using ipv6 exit nodes? I don't see them on check.torproject.org, but I've seen them elsewhere. This behavior is new and I don't understand how IPV6 is being used with Tor when it is disabled on my network.</p>
<p>What and where do I modify ipv6 values in Tor settings somewhere so they aren't used? Especially the line about preferring ipv6 traffic. I don't want any ipv6 used. TIA</p>
<p>[1] In fact, my router cannot use IPV6 technology.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269895"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269895" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  pastly
  </article>
    <div class="comment-header">
      <p class="comment__submitted">pastly said:</p>
      <p class="date-time">July 20, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269872" class="permalink" rel="bookmark">I have IPV6 disabled on my…</a> by <span>IPV6 Exit Nodes? (not verified)</span></p>
    <a href="#comment-269895">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269895" class="permalink" rel="bookmark">&gt; I don&#039;t understand how…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; I don't understand how IPV6 is being used with Tor when it is disabled on my network.</p>
<p>The IP version(s) that your local network supports is irrelevant to the IP version(s) that an exit node's network supports. Remember: Tor transports the bodies of TCP packets. If you connect to your guard with IPv4 and the exit makes a connection to your desired destination with IPv1000, it doesn't matter.</p>
<p>I'm not sure why you think you should disable IPv6. Could you elaborate? And you should leave advanced options like these alone if you have a significant adversary. Changing them will make your behavior look different and a big powerful adversary might be able to notice that.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-269968"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269968" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Otmar (not verified)</span> said:</p>
      <p class="date-time">July 23, 2017</p>
    </div>
    <a href="#comment-269968">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269968" class="permalink" rel="bookmark">High Tor,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>High Tor,<br />
A year ago I found possibilities to go into darknet with Your site. I like it. Regarding Your project I would be able to translate into german. I'm no professional hacker ( who will guide me? ) and my possibilities are now to use some time into translating. I'm german, 54 years old and living alone. My present used email is <a href="mailto:o.pongratz@gmx.de" rel="nofollow">o.pongratz@gmx.de</a><br />
Tell me if You like me to work with You, please</p>
<p>Regards<br />
Otmar</p>
</div>
  </div>
</article>
<!-- Comment END -->
