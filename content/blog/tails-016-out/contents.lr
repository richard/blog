title: Tails 0.16 is out!
---
pub_date: 2013-01-11
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 0.16, is out.</p>

<p>All users must upgrade as soon as possible.</p>

<p><a href="https://tails.boum.org/download/" rel="nofollow">Download it now.</a></p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>Minor improvements
<ul>
<li>Replace the too-easy-to-misclick shutdown button with a better "Shutdown Helper" applet.</li>
<li>Display <span class="geshifilter"><code class="php geshifilter-php">~<span style="color: #339933;">/</span>Persistent</code></span> in GNOME Places and Gtk file chooser.</li>
<li>Install dictionaries for a few languages.</li>
<li>Set Unsafe Browser's window title to "Unsafe Browser".</li>
<li>Install ekeyd to support the EntropyKey.</li>
<li>Install font for Sinhala script.</li>
<li>Update Poedit to 1.5.4.</li>
<li>Expose Vidalia's "broken onion" icon less.</li>
<li>Hide the persistence setup launchers in kiosk mode.</li>
</ul>
</li>
<li>Bugfixes
<ul>
<li>Disable IPv6 on all network interfaces. This is a workaround for the IPv6 link-local multicast leak that was recently discovered.</li>
<li>Tails may previously have been able to list GPT partitions labelled "TailsData" on hard drives (!) as valid persistence volumes... this is now fixed.</li>
<li>Fix SCIM in the autostarted web browser.</li>
<li>Talk of DVD, not of CD, in the shutdown messages.</li>
<li>Make tordate work in bridge mode with an incorrect clock.</li>
</ul>
</li>
<li>Iceweasel
<ul>
<li>Update iceweasel to 10.0.12esr-1+tails1.</li>
<li>Set the homepage to the news section on the Tails website.</li>
<li>Hide the iceweasel add-on bar by default.</li>
<li>Don't hide the AdBlock-Plus button in the add-on bar anymore.</li>
<li>Don't install xul-ext-monkeysphere anymore.</li>
</ul>
</li>
<li>Localization
<ul>
<li>tails-greeter: add German translation, update Portuguese (Brasil) and Russian ones.</li>
<li>tails-persistence-setup: update French, German and Italian translations.</li>
</ul>
</li>
</ul>

<p>Plus the usual bunch of bug reports and minor improvements.</p>

<p>See the <a href="http://git.immerda.ch/?p=amnesia.git;a=blob_plain;f=debian/changelog;hb=refs/tags/0.16" rel="nofollow">online changelog</a> for technical details.</p>

<p><strong>Want to try it or upgrade?</strong></p>

<p>See the <a href="https://tails.boum.org/getting_started/" rel="nofollow">Getting started</a> page.</p>

<p>As no software is ever perfect, we maintain a list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">problems that affects the last release of Tails</a>.</p>

<p><strong>What's coming up?</strong></p>

<p>See the <a href="https://mailman.boum.org/pipermail/tails-dev/2013-January/002427.html" rel="nofollow">release schedule for Tails 0.17</a>.</p>

<p>Have a look to our <a href="https://tails.boum.org/contribute/roadmap/" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Would you want to help? As explained in our <a href="https://tails.boum.org/contribute/" rel="nofollow">"how to contribute" documentation</a>, there are many ways <strong>you</strong> can contribute to Tails. If you want to help, come talk to us!</p>

