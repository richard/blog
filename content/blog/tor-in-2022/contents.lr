title: Tor in 2022
---
author: isabela
---
pub_date: 2021-12-21
---
categories:

community
fundraising
---
summary: It has become a tradition for me to write a blog post at the end of each year, sharing my vision for the Tor Project for the upcoming year. Before talking about what I see for us in 2022, I want to reflect on 2021 and how this has been a year of resilience for Tor.
---
body:

It has become a tradition for me to write a blog post at the end of each year, sharing my vision for the Tor Project for the upcoming year. Before talking about what I see for us in 2022, I want to reflect on 2021 and how this has been a year of resilience for Tor.

## Reflecting on a year of resilience

I’m very proud of every single person who contributed to Tor, the Tor Project staff, our core contributors, our community, and our supporters. 2020 was a year of sacrifice, but none of the stones thrown in our way stopped us from looking ahead and dreaming of a greater future. And in 2021, we bounced back to continue to shape this greater future.

The Tor Project is now financially [stable and healthy](https://blog.torproject.org/transparency-openness-and-our-2019-and-2020-financials/) after a couple of years of challenge. This is due to all the support we received, from each one of you who never gave up on the Tor Project, on our mission, and the power of Tor. This year, after layoffs in 2020, we were financially healthy and able to hire again, bringing amazing, skilled people to our teams.

With a healthy team, we were able to ship very important projects like bringing [Snowflake to Tor Browser stable](https://blog.torproject.org/new-release-tor-browser-105/); [discontinuing Tor launcher and replacing it with the connect page](https://blog.torproject.org/new-release-tor-browser-105/), and [launching the Tor Forum](https://blog.torproject.org/tor-forum-a-new-discussion-platform/), a tool to better support our community and users.

Our project to bring congestion control to the Tor network has made tremendous progress, and we’re close to seeing these features in Tor stable release, thanks to the work done on the [Shadow simulator](https://github.com/shadow), which allows us to test and calibrate new features for the network. [Arti is now on its 0.0.2 release](https://forum.torproject.net/t/arti-0-0-2-is-released-api-groundwork-refactoring-config-and-optimism/939). Tens of thousands of users answered our [usability surveys](https://blog.torproject.org/learning-more-about-tor-users/), giving us great insight on how to improve our tools. I’ve just named a few of the great things we accomplished in 2021.

## Looking forward to 2022

My vision for 2022 is to keep Tor on this track, and our users are our priority when building this strategy.

We will continue to work on Arti so that Tor is on the right path for the future, and we will simultaneously continue to make improvements to the current Tor daemon code. Our congestion control project will make a big improvement in user experience on the Tor network and we want you to benefit from it right away. We already knew that the speed of page loads and downloads are big issues for our users, and our recent usability survey confirmed that. You’ve made it clear that it’s important for us to address speed on the network and by this time in 2022, we have high hopes that you will experience improvements.

As I write this blog post, our team and our community is dealing with [new censorship attacks from Russia](https://blog.torproject.org/tor-censorship-in-russia/). When a user is facing censorship against the Tor network, it can be difficult for them to understand why they can’t connect and how exactly to change their Tor Browser configuration to circumvent this censorship. Our Anti-Censorship, UX, and Application teams have been working on this problem for a long time, and in 2022, we will ship a completely new experience that will automate the censorship detection and circumvention process, simplifying connecting to Tor for users who need it the most.

In 2022 we will also begin the work to provide a better experience for users on mobile, starting with the Android platform, the most used mobile operating system in the world. We know that the user experience of Tor Browser for Android is different from Tor Browser for desktop. Many of the services you use in a browser on desktop (by visiting their website) have become a stand-alone app on mobile. We are doing research to better understand users' needs in this environment and will begin to design a Tor app for Android that will help you route your app connections through Tor, a ‘VPN-like’ app. This way, we can offer the robust protection of an encrypted and decentralized network like Tor for a wider variety of use cases.

The success of all of the above depends on ensuring that the Tor network is healthy. This year, our team has improved existing and created new tools that help us monitor the Tor network for malicious relay activity so that we can remove them from the network. In 2022, we will continue to improve these tools and we will work on automating some of the steps in this process so that we can take faster actions to protect the network. We will also roll out a series of initiatives to better organize our relay operator community and strengthen the relationship and trust between the Tor Project and the relay operators. This way our network, which is 100% based on volunteer (community) support, can grow and stay safe.

I see that 2022 will not be a year of resilience, but a year of passion. Our passion and your passion for the mission of the Tor Project is what keeps this fire on. Millions of people all around the world depend on our technology, and it is the passion of the people behind it, like you, that makes Tor possible.

## Thank you!

To end this post, I want to thank you for supporting Tor and for sharing this passion with us. **I want ask those who can to make a donation to the Tor Project.** Your contribution is key for us to stay on track and achieve our goals for 2022. You can donate using [cryptocurrency](https://donate.torproject.org/cryptocurrency) or old fashion fiat [currency](https://donate.torproject.org) :) and get some amazing swag, including a limited edition DEF CON badge!
