title: 64-bit GNU/Linux Tor Browser Bundles updated
---
pub_date: 2013-11-20
---
author: erinn
---
tags:

tor browser bundle
tor browser
tbb
hotfix
---
categories: applications
---
_html_body:

<p>It turns out that the 64-bit bundles were a bit crashy because of a change in the way they were built. This change has been reverted and I've updated the stable and RC versions of the 2.x series of GNU/Linux Tor Browser Bundles.</p>

<p>Direct links:<br />
<a href="https://www.torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-x86_64-2.3.25-16-dev-en-US.tar.gz" rel="nofollow">Stable 64-bit GNU/Linux Tor Browser Bundle</a> (<a href="https://www.torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-x86_64-2.3.25-16-dev-en-US.tar.gz.asc" rel="nofollow">sig</a>)<br />
<a href="https://www.torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-x86_64-2.4.18-rc-2-dev-en-US.tar.gz" rel="nofollow">RC 64-bit GNU/Linux Tor Browser Bundle</a> (<a href="https://www.torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-x86_64-2.4.18-rc-2-dev-en-US.tar.gz.asc" rel="nofollow">sig</a>)</p>

<p><strong>Tor Browser Bundle (2.3.25-16); suite=linux</strong></p>

<ul>
<li>Update 64-bit Linux's mozconfig to --disable-optimize so Tor Browser will<br />
    stop crashing (closes: #10195)</li>
</ul>

<p><strong>Tor Browser Bundle (2.4.18-rc-2); suite=linux</strong></p>

<ul>
<li>Update 64-bit Linux's mozconfig to --disable-optimize so Tor Browser will<br />
    stop crashing (closes: #10195)</li>
</ul>

---
_comments:

<a id="comment-38425"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38425" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 21, 2013</p>
    </div>
    <a href="#comment-38425">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38425" class="permalink" rel="bookmark">Thank you for the update.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for the update.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-38429"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38429" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 21, 2013</p>
    </div>
    <a href="#comment-38429">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38429" class="permalink" rel="bookmark">You forgot to update the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You forgot to update the version string on this page: <a href="https://www.torproject.org/download/download-easy.html.en" rel="nofollow">https://www.torproject.org/download/download-easy.html.en</a> - it still says 2.3.25-15 under Linux but the download link points to 2.3.25-16.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-38621"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38621" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 23, 2013</p>
    </div>
    <a href="#comment-38621">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38621" class="permalink" rel="bookmark">The new</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The new <a href="https://check.torproject.org/RecommendedTBBVersions" rel="nofollow">https://check.torproject.org/RecommendedTBBVersions</a><br />
is very problematic. It contains a double entry for Linux-stable, but there's no way to programmatically understand which one is for i686 and which one is for x86_64.<br />
I think that the best option is make a symlink (or a whole new build) so that<br />
<a href="https://www.torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-i686-2.3.25-16-dev-en-US.tar.gz" rel="nofollow">https://www.torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-…</a><br />
become a working link.<br />
Until that, every automatic download-and-update method (such as <a href="https://github.com/micahflee/torbrowser-launcher" rel="nofollow">https://github.com/micahflee/torbrowser-launcher</a> ) will be broken</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-38774"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38774" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 24, 2013</p>
    </div>
    <a href="#comment-38774">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38774" class="permalink" rel="bookmark">Erinn &lt;3
Thank you and the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Erinn &lt;3</p>
<p>Thank you and the rest of the team.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-46322"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-46322" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 26, 2014</p>
    </div>
    <a href="#comment-46322">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-46322" class="permalink" rel="bookmark">Are you guys going to make a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are you guys going to make a 64 bit tor for Windows?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-46330"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-46330" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 26, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-46322" class="permalink" rel="bookmark">Are you guys going to make a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-46330">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-46330" class="permalink" rel="bookmark">I hear the 32-bit one for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I hear the 32-bit one for Windows works just fine on 64-bit.</p>
<p>(This isn't the case for the 32-bit Linux on 64-bit Linux.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
