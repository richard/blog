title: Towards a Tor Censorship Analysis Tool
---
pub_date: 2013-02-06
---
author: phw
---
tags:

censorship
measurement
---
categories:

circumvention
metrics
---
_html_body:

<p>The Tor network is <a href="https://censorshipwiki.torproject.org" rel="nofollow">documented to be blocked</a> in several countries. Analyzing and circumventing these blocks typically requires detailed packet traces or access to machines inside censoring countries. Both, however, are not always easy to acquire:</p>

<ol>
<li>
Network traces are problematic for two reasons. First, they are difficult to obtain since they require the cooperation of users within censoring countries. Second, they are hard to anonymize and must not fall into wrong hands. Derived information, such as <a href="https://media.torproject.org/image/blog-images/2012-05-31-ethiopia-dpi-blocking-of-tor.png" rel="nofollow">flow diagrams</a>, is typically safe to publish but frequently lacks important information.
</li>
<li>
The alternative to network traces is to gain access to machines inside the censoring regime. This approach turns out to be difficult as well; mostly due to the lack of volunteers who could provide machines or the lack of VPS providers and open SOCKS proxies.
</li>
</ol>

<p>These problems show that there is a strong need for a lightweight tool which can assist in analyzing censorship events. This tool should be run by censored users and perform several tests to gain a rough understanding of how and if Tor could be blocked in the respective network. The results of these tests should make it back to the Tor project and would be used to improve circumvention technology such as <a href="https://www.torproject.org/projects/obfsproxy.html.en" rel="nofollow">obfsproxy</a> and to <a href="https://censorshipwiki.torproject.org" rel="nofollow">document censorship</a>.</p>

<p>We created a <a href="https://research.torproject.org/techreports/censorship-analysis-tool-2013-02-06.pdf" rel="nofollow">technical report which discusses the design requirements for such a censorship analysis tool</a>.  We list the desired features, discuss how they can be implemented and we give a rough overview of the software design. After all, this technical report should serve as basis for the development and deployment of the censorship analysis tool.</p>

---
_comments:

<a id="comment-18733"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18733" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 16, 2013</p>
    </div>
    <a href="#comment-18733">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18733" class="permalink" rel="bookmark">I do hope you have the good</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I do hope you have the good sense to keep such a tool completely separate from your other downloads, packages, bundles and all. The last thing a Tor user wants is some sort of built-in remote access for a 3rd party.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-18753"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18753" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">February 18, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18733" class="permalink" rel="bookmark">I do hope you have the good</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-18753">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18753" class="permalink" rel="bookmark">Yes, the idea is to offer</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, the idea is to offer this tool as a separate download. After all, most Tor users will hopefully never need it. Also, this has <b>nothing</b> to do with remote access or other insanities. The paper should clarify what it does and does not do.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-18993"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18993" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 28, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18733" class="permalink" rel="bookmark">I do hope you have the good</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-18993">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18993" class="permalink" rel="bookmark">Oh wow, you really should</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Oh wow, you really should read the article before making comments in future.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-18987"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18987" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 28, 2013</p>
    </div>
    <a href="#comment-18987">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18987" class="permalink" rel="bookmark">Unnamed, IP 92.38.209.239,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Unnamed, IP 92.38.209.239, $AB0850B7C517B0C9EC17F1CDC73C6CC425BFFCC4, Russia, blocks arbitrary websites.</p>
</div>
  </div>
</article>
<!-- Comment END -->
