title: Roger's HAR2009 talk on Tor performance
---
pub_date: 2009-08-19
---
author: arma
---
tags:

talks
performance
videos
---
categories:

community
network
---
_html_body:

<p>Jake, Mike, Karsten, Sebastian, and I attended <a href="https://har2009.org/" rel="nofollow">Hacking at Random</a> last week in The Netherlands. I did a <a href="https://har2009.org/program/events/33.en.html" rel="nofollow">talk</a> on Tor performance challenges — basically walking through the key pieces of the <a href="https://blog.torproject.org/blog/why-tor-is-slow" rel="nofollow">"Why Tor is Slow"</a> document that we wrote in March.</p>

<p>As usual with European hacking cons, they produced a really well-done video just days after my talk. So if you want to get the highlights on what we're doing to speed up Tor and what roadblocks remain, <a href="http://freehaven.net/~arma/har2009_Why_Tor_is_slow.mp4" rel="nofollow">take a look at the video</a> and also the <a href="http://freehaven.net/~arma/slides-har2009.pdf" rel="nofollow">slides that come with it</a>.</p>

---
_comments:

<a id="comment-2170"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2170" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 19, 2009</p>
    </div>
    <a href="#comment-2170">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2170" class="permalink" rel="bookmark">Torrent</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There's a torrent of this up at <a href="http://www.johntowery.com/har2009_Why_Tor_is_slow.mp4.torrent" rel="nofollow">http://www.johntowery.com/har2009_Why_Tor_is_slow.mp4.torrent</a></p>
<p>It uses the hidden tracker and should work for both Tor and non-tor users.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2171"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2171" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 19, 2009</p>
    </div>
    <a href="#comment-2171">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2171" class="permalink" rel="bookmark">https://blog.torproject.org/b</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://blog.torproject.org/blog/why-tor-is-slow" rel="nofollow">https://blog.torproject.org/blog/why-tor-is-slow</a> appears to be a broken link.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2172"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2172" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 19, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2171" class="permalink" rel="bookmark">https://blog.torproject.org/b</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2172">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2172" class="permalink" rel="bookmark">Re: https://blog.torproject.org/b</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Gah. You're right, I broke it when updating it to include a link to this post.</p>
<p>Fixed now. Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2173"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2173" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>thebard (not verified)</span> said:</p>
      <p class="date-time">August 19, 2009</p>
    </div>
    <a href="#comment-2173">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2173" class="permalink" rel="bookmark">When you talk, in the paper,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When you talk, in the paper, about increasing support and such for people hosting relays, I think it would be of great benefit if you provided feedback on how well their relays are performing, with advice on how to improve them in cases of "misbehaving" relays. There are a lot of people out there supporting TOR who are practically computer noobs, and so won't know if their relay is set up correctly or not.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2182"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2182" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 21, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2173" class="permalink" rel="bookmark">When you talk, in the paper,</a> by <span>thebard (not verified)</span></p>
    <a href="#comment-2182">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2182" class="permalink" rel="bookmark">re: relay op support</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>These are good points.  Most "misbehaving" relays have turned out to be misconfiguration; sometimes in obvious ways.</p>
<p>We're creating a relay operator-only mailing list to provide a relay operator to relay operator support channel.  Many relay ops ask us questions directly vs on our other mailing lists because they are publicly archived, and can have a low signal to noise ratio.</p>
<p>Another tool we're working on is called Tor Weather.  It's a tool designed to email the contact when the relay goes down for a period of time, or if you're determined to be a bad exit through user feedback or automated scanning.</p>
<p>These two steps should help many of the new relays.  Another option is to email a new relay operator and offer a few links about how to find support and address common questions.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-5681"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5681" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 16, 2010</p>
    </div>
    <a href="#comment-5681">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5681" class="permalink" rel="bookmark">The torrent for the video</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The torrent for the video does not work. The video doesn't work either when you download it by itself. I have quicktime updated to the most recent version. It says something about an error in the video file. When you play it in the most recent version of Divx, there is video, but no sound.</p>
</div>
  </div>
</article>
<!-- Comment END -->
