title: Tor 0.3.2.6-alpha is released, with security updates
---
pub_date: 2017-12-01
---
author: nickm
---
tags: alpha release
---
categories: releases
---
summary:

This version of Tor is the latest in the 0.3.2 alpha series. It includes fixes for several important security issues. All Tor users should upgrade to this release, or to one of the other releases coming out today. (The next announcement will be about the stable releases.)

---
_html_body:

<p>This version of Tor is the latest in the 0.3.2 alpha series. It includes fixes for several important security issues. All Tor users should upgrade to this release, or to one of the other releases coming out today. (The next announcement will be about the stable releases.)</p>
<p>You can download the source from the usual place on the website. Binary packages should be available soon.</p>
<p>These releases fix the following security bugs. For more information<br />
on each one, see the links from<br />
<a data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://trac.torproject.org/projects/tor/wiki/TROVE&amp;source=gmail&amp;ust=1512224010614000&amp;usg=AFQjCNF5qNawdA642O5uJs9NHhjSF4Afrw" href="https://trac.torproject.org/projects/tor/wiki/TROVE" rel="noreferrer" target="_blank">https://trac.torproject.org/<wbr></wbr>projects/tor/wiki/TROVE</a></p>
<p>TROVE-2017-009: Replay-cache ineffective for v2 onion services<br />
TROVE-2017-010: Remote DoS attack against directory authorities<br />
TROVE-2017-011: An attacker can make Tor ask for a password<br />
TROVE-2017-012: Relays can pick themselves in a circuit path<br />
TROVE-2017-013: Use-after-free in onion service v2</p>
<h2>Changes in version 0.3.2.6-alpha - 2017-12-01</h2>
<ul>
<li>Major bugfixes (security):
<ul>
<li>Fix a denial of service bug where an attacker could use a malformed directory object to cause a Tor instance to pause while OpenSSL would try to read a passphrase from the terminal. (Tor instances run without a terminal, which is the case for most Tor packages, are not impacted.) Fixes bug <a href="https://bugs.torproject.org/24246">24246</a>; bugfix on every version of Tor. Also tracked as TROVE-2017-011 and CVE-2017-8821. Found by OSS-Fuzz as testcase 6360145429790720.</li>
<li>Fix a denial of service issue where an attacker could crash a directory authority using a malformed router descriptor. Fixes bug <a href="https://bugs.torproject.org/24245">24245</a>; bugfix on 0.2.9.4-alpha. Also tracked as TROVE-2017-010 and CVE-2017-8820.</li>
<li>When checking for replays in the INTRODUCE1 cell data for a (legacy) onion service, correctly detect replays in the RSA- encrypted part of the cell. We were previously checking for replays on the entire cell, but those can be circumvented due to the malleability of Tor's legacy hybrid encryption. This fix helps prevent a traffic confirmation attack. Fixes bug <a href="https://bugs.torproject.org/24244">24244</a>; bugfix on 0.2.4.1-alpha. This issue is also tracked as TROVE-2017-009 and CVE-2017-8819.</li>
</ul>
</li>
<li>Major bugfixes (security, onion service v2):
<ul>
<li>Fix a use-after-free error that could crash v2 Tor onion services when they failed to open circuits while expiring introduction points. Fixes bug <a href="https://bugs.torproject.org/24313">24313</a>; bugfix on 0.2.7.2-alpha. This issue is also tracked as TROVE-2017-013 and CVE-2017-8823.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major bugfixes (security, relay):
<ul>
<li>When running as a relay, make sure that we never build a path through ourselves, even in the case where we have somehow lost the version of our descriptor appearing in the consensus. Fixes part of bug <a href="https://bugs.torproject.org/21534">21534</a>; bugfix on 0.2.0.1-alpha. This issue is also tracked as TROVE-2017-012 and CVE-2017-8822.</li>
<li>When running as a relay, make sure that we never choose ourselves as a guard. Fixes part of bug <a href="https://bugs.torproject.org/21534">21534</a>; bugfix on 0.3.0.1-alpha. This issue is also tracked as TROVE-2017-012 and CVE-2017-8822.</li>
</ul>
</li>
<li>Minor feature (relay statistics):
<ul>
<li>Change relay bandwidth reporting stats interval from 4 hours to 24 hours in order to reduce the efficiency of guard discovery attacks. Fixes ticket <a href="https://bugs.torproject.org/23856">23856</a>.</li>
</ul>
</li>
<li>Minor features (directory authority):
<ul>
<li>Add an IPv6 address for the "bastet" directory authority. Closes ticket <a href="https://bugs.torproject.org/24394">24394</a>.</li>
</ul>
</li>
<li>Minor bugfixes (client):
<ul>
<li>By default, do not enable storage of client-side DNS values. These values were unused by default previously, but they should not have been cached at all. Fixes bug <a href="https://bugs.torproject.org/24050">24050</a>; bugfix on 0.2.6.3-alpha.</li>
</ul>
</li>
</ul>

