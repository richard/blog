title: Tor 0.2.5.12 and 0.2.6.7 are released
---
pub_date: 2015-04-07
---
author: arma
---
tags: tor releases
---
categories: releases
---
_html_body:

<p>Tor 0.2.5.12 and 0.2.6.7 fix two security issues that could be used by an attacker to crash hidden services, or crash clients visiting hidden services. Hidden services should upgrade as soon as possible; clients should upgrade whenever packages become available.</p>

<p>These releases also contain two simple improvements to make hidden services a bit less vulnerable to denial-of-service attacks.</p>

<p>We also made a Tor 0.2.4.27 release so that Debian stable can easily integrate these fixes.</p>

<p>The Tor Browser team is currently evaluating whether to put out a new Tor Browser stable release with these fixes, or wait until next week for their scheduled next stable release. (The bugs can introduce hassles for users, but we don't currently view them as introducing any threats to anonymity.)</p>

<h2>Changes in version 0.2.5.12 - 2015-04-06</h2>

<ul>
<li>Major bugfixes (security, hidden service):
<ul>
<li>Fix an issue that would allow a malicious client to trigger an assertion failure and halt a hidden service. Fixes bug 15600; bugfix on 0.2.1.6-alpha. Reported by "disgleirio".
    </li>
<li>Fix a bug that could cause a client to crash with an assertion failure when parsing a malformed hidden service descriptor. Fixes bug 15601; bugfix on 0.2.1.5-alpha. Found by "DonnchaC".
  </li>
</ul>
</li>
<li>Minor features (DoS-resistance, hidden service):
<ul>
<li>Introduction points no longer allow multiple INTRODUCE1 cells to arrive on the same circuit. This should make it more expensive for attackers to overwhelm hidden services with introductions. Resolves ticket 15515.
  </li>
</ul>
</li>
</ul>

<h2>Changes in version 0.2.6.7 - 2015-04-06</h2>

<ul>
<li>Major bugfixes (security, hidden service):
<ul>
<li> Fix an issue that would allow a malicious client to trigger an assertion failure and halt a hidden service. Fixes bug 15600; bugfix on 0.2.1.6-alpha. Reported by "disgleirio".
    </li>
<li> Fix a bug that could cause a client to crash with an assertion failure when parsing a malformed hidden service descriptor. Fixes bug 15601; bugfix on 0.2.1.5-alpha. Found by "DonnchaC".
  </li>
</ul>
</li>
<li> Minor features (DoS-resistance, hidden service):
<ul>
<li> Introduction points no longer allow multiple INTRODUCE1 cells to arrive on the same circuit. This should make it more expensive for attackers to overwhelm hidden services with introductions.  Resolves ticket 15515.
    </li>
<li> Decrease the amount of reattempts that a hidden service performs when its rendezvous circuits fail. This reduces the computational cost for running a hidden service under heavy load. Resolves ticket 11447.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-92075"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92075" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 08, 2015</p>
    </div>
    <a href="#comment-92075">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92075" class="permalink" rel="bookmark">You might also want to fix a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You might also want to fix a crash thats happening in TorBrowser when stating invalid SocksPort such as "SocksPort 192.168.55.90:9182" when the local ip address actually is 192.168.54.90.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-92088"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92088" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 08, 2015</p>
    </div>
    <a href="#comment-92088">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92088" class="permalink" rel="bookmark">Thanks for another great</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for another great release</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-92109"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92109" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 09, 2015</p>
    </div>
    <a href="#comment-92109">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92109" class="permalink" rel="bookmark">will you please update the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>will you please update the tor installers located at <a href="https://dist.torproject.org/win32/" rel="nofollow">https://dist.torproject.org/win32/</a>? there is no 0.2.5.x option there, why not?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-92130"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92130" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">April 09, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-92109" class="permalink" rel="bookmark">will you please update the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-92130">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92130" class="permalink" rel="bookmark">You can find the new expert</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can find the new expert bundle for Windows linked from the download page.</p>
<p>The old installers aren't being maintained, because they would require somebody to build and maintain them, and we currently have no such somebodies.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-92368"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92368" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 16, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-92368">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92368" class="permalink" rel="bookmark">What about the command line</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about the command line functionality?  I was using 0.2.4.21 on Windows Vista until I decided to upgrade to 0.2.5.12 and found that there was no stdout messages, nor did -version/-h/-help work either.  I downloaded the zip file (expert bundle).</p>
<p>I much prefer the way it worked before and I am very hesitant to rely on the Tor Browser (though I have used it).</p>
<p>Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-92111"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92111" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 09, 2015</p>
    </div>
    <a href="#comment-92111">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92111" class="permalink" rel="bookmark">Where can I find the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Where can I find the installer for latest tor on win32 ?<br />
Im running a tor relay on windows.<br />
The expert bundle on main download page contains a small tor.exe and some libraries that I dont know how to use in windows, so I need the win32 installer.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-92151"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92151" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 10, 2015</p>
    </div>
    <a href="#comment-92151">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92151" class="permalink" rel="bookmark">&quot;The old installers aren&#039;t</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"The old installers aren't being maintained, because they would require somebody to build and maintain them, and we currently have no such somebodies."</p>
<p>Accepting that the maintaining an installer for Tor alone is a significant burden on top of maintinaing the installer for the more complicated packages:</p>
<p>There at least ought to be a link on the download page to something explaining this, and providing instructions on what to do with the "new" expert bundle.  Alternatively, this could be in a README included in the zip file.</p>
<p>As I remember it, what has become the "expert" bundle was originally the main way that Tor was distributed for Windows.  Whether I remember correctly or not, there are plenty of people who don't use, and don't want to use, the Tor browser bundle, but aren't "experts" at manually installing Tor on Windows.  It's also not obvious what needs to be done about torrc.  Simply unzipping the file and running the tor.exe does not produce the expected previous behavior so it's not unreasonable for people to be confused.</p>
<p>If this information exists, I haven't come across it.  The only thing I've found are a few disconnected comments in bug reports, but nothing from anyone in the Tor project itself.</p>
<p>I do appreciate the work done by the maintainers and developers, but there needs to be some alternative to building from source for people who want only Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-92982"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92982" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 06, 2015</p>
    </div>
    <a href="#comment-92982">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92982" class="permalink" rel="bookmark">i can use 0.2.4 but not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i can use 0.2.4 but not 0.2.6.7<br />
i don't know how to us tor.exe.</p>
</div>
  </div>
</article>
<!-- Comment END -->
