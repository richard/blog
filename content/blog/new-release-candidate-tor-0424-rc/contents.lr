title: New Release Candidate: Tor 0.4.2.4-rc
---
pub_date: 2019-11-15
---
author: nickm
---
tags: release candidate
---
categories: releases
---
_html_body:

<p>There's a new release candidate available for download. If you build Tor from source, you can download the source code for 0.4.2.4-rc from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release likely by December 3.</p>
<p>Remember, this is a release candidate: there may still be more bugs here than usual. We'd love to know about any new ones, so that we can try to get them fixed before we call this series stable.</p>
<p>Tor 0.4.2.4-rc is the first release candidate in its series. It fixes several bugs from earlier versions, including a few that would result in stack traces or incorrect behavior.</p>
<h2>Changes in version 0.4.2.4-rc - 2019-11-15</h2>
<ul>
<li>Minor features (build system):
<ul>
<li>Make pkg-config use --prefix when cross-compiling, if PKG_CONFIG_PATH is not set. Closes ticket <a href="https://bugs.torproject.org/32191">32191</a>.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the November 6 2019 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/32440">32440</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor bugfixes (client, onion service v3):
<ul>
<li>Fix a BUG() assertion that occurs within a very small race window between when a client intro circuit opens and when its descriptor gets cleaned up from the cache. The circuit is now closed early, which will trigger a re-fetch of the descriptor and continue the connection. Fixes bug <a href="https://bugs.torproject.org/28970">28970</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (code quality):
<ul>
<li>Fix "make check-includes" so it runs correctly on out-of-tree builds. Fixes bug <a href="https://bugs.torproject.org/31335">31335</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (configuration):
<ul>
<li>Log the option name when skipping an obsolete option. Fixes bug <a href="https://bugs.torproject.org/32295">32295</a>; bugfix on 0.4.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (crash):
<ul>
<li>When running Tor with an option like --verify-config or --dump-config that does not start the event loop, avoid crashing if we try to exit early because of an error. Fixes bug <a href="https://bugs.torproject.org/32407">32407</a>; bugfix on 0.3.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (directory):
<ul>
<li>When checking if a directory connection is anonymous, test if the circuit was marked for close before looking at its channel. This avoids a BUG() stacktrace if the circuit was previously closed. Fixes bug <a href="https://bugs.torproject.org/31958">31958</a>; bugfix on 0.4.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (shellcheck):
<ul>
<li>Fix minor shellcheck errors in the git-*.sh scripts. Fixes bug <a href="https://bugs.torproject.org/32402">32402</a>; bugfix on 0.4.2.1-alpha.</li>
<li>Start checking most scripts for shellcheck errors again. Fixes bug <a href="https://bugs.torproject.org/32402">32402</a>; bugfix on 0.4.2.1-alpha.</li>
</ul>
</li>
<li>Testing (continuous integration):
<ul>
<li>Use Ubuntu Bionic images for our Travis CI builds, so we can get a recent version of coccinelle. But leave chutney on Ubuntu Trusty, until we can fix some Bionic permissions issues (see ticket <a href="https://bugs.torproject.org/32240">32240</a>). Related to ticket <a href="https://bugs.torproject.org/31919">31919</a>.</li>
<li>Install the mingw OpenSSL package in Appveyor. This makes sure that the OpenSSL headers and libraries match in Tor's Appveyor builds. (This bug was triggered by an Appveyor image update.) Fixes bug <a href="https://bugs.torproject.org/32449">32449</a>; bugfix on 0.3.5.6-rc.</li>
<li>In Travis, use Xcode 11.2 on macOS 10.14. Closes ticket <a href="https://bugs.torproject.org/32241">32241</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-285546"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285546" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>What&#039;s going on? (not verified)</span> said:</p>
      <p class="date-time">November 17, 2019</p>
    </div>
    <a href="#comment-285546">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285546" class="permalink" rel="bookmark">Read the DDOS fix was coming…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Read the DDOS fix was coming in 4.2 no mention of it. Is the fix still planned for this? How can onion operators use it when it is added</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285607"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285607" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 19, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285546" class="permalink" rel="bookmark">Read the DDOS fix was coming…</a> by <span>What&#039;s going on? (not verified)</span></p>
    <a href="#comment-285607">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285607" class="permalink" rel="bookmark">I don&#039;t think there is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't think there is something like "the DDOS" fix. There a a multitude of denial-of-service attacks feasible against the Tor network. As a result there has been a stream of mitigations against denial-of-service attacks in recent versions of Tor and not just in 0.4.2.</p>
<p>However, there is a major DoS defense included in 0.4.2 to mitigate attacks against onion services. Perhaps, you meant this defense. Since this blog is only about the latest alpha release in the 0.4.2 series (i.e changes since 0.4.2.3-alpha), you can't find anything about it in this post. To get a complete picture of all changes within the 0.4.2 series, check out the <a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=tor-0.4.2.4-rc" rel="nofollow">release notes</a> of the earlier alpha releases. In particular <a href="https://blog.torproject.org/new-alpha-release-tor-0421-alpha" rel="nofollow">0.4.2.1-alpha</a> should be of interest. Details about fine-tuning the defense can be found in <a href="https://2019.www.torproject.org/docs/tor-manual.html#_denial_of_service_mitigation_options" rel="nofollow">the manpages</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285712"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285712" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Rich (not verified)</span> said:</p>
      <p class="date-time">November 25, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285607" class="permalink" rel="bookmark">I don&#039;t think there is…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-285712">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285712" class="permalink" rel="bookmark">When will 0.4.2 be released?…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When will 0.4.2 be released? not beta or rc version.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285726"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285726" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">November 26, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285712" class="permalink" rel="bookmark">When will 0.4.2 be released?…</a> by <span>Rich (not verified)</span></p>
    <a href="#comment-285726">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285726" class="permalink" rel="bookmark">We&#039;re aiming for releasing a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We're aiming for releasing a stable 0.4.2.x somewhere around 15 December 2019, but of course it's impossible to say for sure: if we find a huge bug that's hard to fix, it'll take longer than that. </p>
<p>(Our intended schedule is at <a href="https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam/CoreTorReleases" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam/Cor…</a> )</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-285895"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285895" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Jorge (not verified)</span> said:</p>
      <p class="date-time">December 06, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285607" class="permalink" rel="bookmark">I don&#039;t think there is…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-285895">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285895" class="permalink" rel="bookmark">Downloading the r.c and with…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Downloading the r.c and with the aim to test it today do I need to add the DoS defense to torrc?</p>
<p>These - DoSCircuitCreationEnabled 0|1|auto, DoSCircuitCreationMinConnections, DoSCircuitCreationRate, DoSCircuitCreationBurst, DoSCircuitCreationDefenseType , DoSCircuitCreationDefenseTimePeriod N seconds|minutes|hours, DoSConnectionEnabled 0|1|auto, DoSConnectionMaxConcurrentCount NUM, DoSConnectionDefenseType NUM, DoSRefuseSingleHopClientRendezvous 0|1|auto</p>
<p>Aim is to help with DDoS attack against hidden service will I use them all  and do I put them inside my torrc file  ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285942"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285942" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  dgoulet
  </article>
    <div class="comment-header">
      <p class="comment__submitted">dgoulet said:</p>
      <p class="date-time">December 09, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285895" class="permalink" rel="bookmark">Downloading the r.c and with…</a> by <span>Jorge (not verified)</span></p>
    <a href="#comment-285942">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285942" class="permalink" rel="bookmark">Those parameters do not…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Those parameters do not apply to hidden service themselves. They only apply if you run a relay. The tor network currently enables some of them with some default values.</p>
<p>If you run a relay and you are unsure here, I would avoid setting any of them.</p>
<p>The hidden service specific DoS defenses are:</p>
<ul>
<li>HiddenServiceEnableIntroDoSDefense</li>
<li>HiddenServiceEnableIntroDoSRatePerSec</li>
<li>HiddenServiceEnableIntroDoSBurstPerSec</li>
</ul>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-285626"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285626" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="DoubleDutch or Swahili or English or Martian - take your pik">DoubleDutch or… (not verified)</span> said:</p>
      <p class="date-time">November 20, 2019</p>
    </div>
    <a href="#comment-285626">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285626" class="permalink" rel="bookmark">Use Ubuntu Bionic images for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Use Ubuntu Bionic images for our Travis CI builds, so we can get a recent version of coccinelle. But leave chutney on Ubuntu Trusty, until we can fix some Bionic permissions issues (see ticket 32240). Related to ticket 31919.</p>
<p>If those words were uttered only a short time ago, you'd be approached by men and woman in white coats.</p>
<p>Its a headscratcher still.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285636"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285636" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">November 21, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285626" class="permalink" rel="bookmark">Use Ubuntu Bionic images for…</a> by <span content="DoubleDutch or Swahili or English or Martian - take your pik">DoubleDutch or… (not verified)</span></p>
    <a href="#comment-285636">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285636" class="permalink" rel="bookmark">My favorite such headline is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>My favorite such headline is the classic "Galaxy Nexus: Android Ice Cream Sandwich Guinea Pig?"</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-285628"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285628" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>sss (not verified)</span> said:</p>
      <p class="date-time">November 20, 2019</p>
    </div>
    <a href="#comment-285628">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285628" class="permalink" rel="bookmark">can anyone link me to a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>can anyone link me to a detailed guide for torify terminal commands?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285661"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285661" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 23, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285628" class="permalink" rel="bookmark">can anyone link me to a…</a> by <span>sss (not verified)</span></p>
    <a href="#comment-285661">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285661" class="permalink" rel="bookmark">What programs can I use with…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><ul>
<li><a href="https://support.torproject.org/about/can-i-use-tor-with/" rel="nofollow">What programs can I use with Tor?</a></li>
</ul>
<p>Note: Those guides are probably old. If you don't find your program there, search the web for how to torify it or connect it to Tor. In general, you configure a <span class="geshifilter"><code class="php geshifilter-php">SocksPort</code></span> in your <a href="https://support.torproject.org/tbb/tbb-editing-torrc/" rel="nofollow">torrc file</a> and then configure your user-facing program to proxy through that local port or the default port, 9050 or 9150.  Beware of your program or protocols possibly leaking data by not always connecting to that port.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285747"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285747" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>sss (not verified)</span> said:</p>
      <p class="date-time">November 29, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285661" class="permalink" rel="bookmark">What programs can I use with…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-285747">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285747" class="permalink" rel="bookmark">thanks! seems i wasn&#039;t…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanks! seems i wasn't searching for the right thing. didn't find much on torifying my whole system but i found lot of tutorials for setting up a socks proxy with tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-285652"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285652" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Mr ¥ (not verified)</span> said:</p>
      <p class="date-time">November 21, 2019</p>
    </div>
    <a href="#comment-285652">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285652" class="permalink" rel="bookmark">So...
One &#039;must&#039; donate in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So...<br />
One 'must' donate in order to avail of free unfettered onion flavoured Web access?<br />
And if one, say, is unemployed or without finances etc...<br />
What then?<br />
Thanks in advance for expected non reply. </p>
<p>Mr.¥</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285673"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285673" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">November 24, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285652" class="permalink" rel="bookmark">So...
One &#039;must&#039; donate in…</a> by <span>Mr ¥ (not verified)</span></p>
    <a href="#comment-285673">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285673" class="permalink" rel="bookmark">Sorry, but I don&#039;t know what…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry, but I don't know what you're talking about.  Tor is free to use, as it always has been.  We're asking for donations, but you don't have to donate to use the software if you don't want to.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285728"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285728" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 26, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285652" class="permalink" rel="bookmark">So...
One &#039;must&#039; donate in…</a> by <span>Mr ¥ (not verified)</span></p>
    <a href="#comment-285728">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285728" class="permalink" rel="bookmark">&gt; if one, say, is unemployed…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; if one, say, is unemployed or without finances etc... What then?</p>
<ul>
<li><a href="https://support.torproject.org/misc/misc-14/" rel="nofollow">How do I volunteer with Tor Project?</a></li>
<li><a href="https://donate.torproject.org/donor-faq" rel="nofollow">38. Can I donate my time?</a></li>
<li><a href="https://2019.www.torproject.org/docs/faq.html.en#WhySlow" rel="nofollow">Why is Tor so slow? (What can you do to help?)</a></li>
<li><a href="https://2019.www.torproject.org/docs/faq.html.en#Funding" rel="nofollow">What would The Tor Project do with more funding?</a></li>
<li><a href="https://2019.www.torproject.org/docs/faq.html.en#EverybodyARelay" rel="nofollow">You should make every Tor user be a relay.</a></li>
<li><a href="https://2019.www.torproject.org/about/overview.html.en#thefutureoftor" rel="nofollow">The future of Tor</a></li>
<li><a href="https://2019.www.torproject.org/getinvolved/volunteer.html.en" rel="nofollow">Volunteer FAQ (Side navigation menu: Get Involved, Set Up Relays, Research)</a></li>
</ul>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-285655"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285655" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 22, 2019</p>
    </div>
    <a href="#comment-285655">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285655" class="permalink" rel="bookmark">Thinking about businesses…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thinking about businesses whose revenue either is made from web admins purchasing defenses or is in direct competition with the Tor network for privacy-minded customers who become fed up with captchas or perceived association with illegal behavior, I hope that Tor Project has mulled over the extent of exit relays that could be surreptitiously operated by such entities.</p>
<p>Consider that Shodan was discovered in 2016 running innocent-looking public NTP servers that harvested connection addresses, including private DNS-absent IPv6 addresses, to run port scans on them. It would be trivial to substitute NTP with tor, connection addresses with web destinations, and port scans with DoS or "unusual traffic from your network."</p>
<p><a href="https://netpatterns.blogspot.com/2016/01/the-rising-sophistication-of-network.html" rel="nofollow">https://netpatterns.blogspot.com/2016/01/the-rising-sophistication-of-n…</a><br />
<a href="https://news.ycombinator.com/item?id=10981002" rel="nofollow">https://news.ycombinator.com/item?id=10981002</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
